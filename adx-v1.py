import math
import pandas as pd
import os.path
import glob
import time
import re
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import time
from datetime import datetime
import csv

# MAIN VARS #
name1 = 'Reagan Rico'
email1 = 'reagan.rico@rtbhouse.com'
ccemails = 'damian.gwozdz@rtbhouse.com'

# CSV READER
csvfiles = glob.glob('*.csv') #Check all CSV in folder

master = []
sendList = []
elem = 0

gx = 0
g = len(csvfiles)

gl = ['ru', 'br', 'apac', 'tr', 'pl', 'jp', 'us']
adxIDList = ['96279664', '124471394', '197761117', '215763014', '244158412', '348485441', '678493859', '749236047'] # JP TBC
adxGeoList = ['RTB House MAIN', 'RTB House RU', 'RTB House BR', 'RTB House APAC', 'RTB House TR', 'RTB House POLAND', 'RTB House JAPAN', 'RTB House US']

adxID = ''
adxGeo = ''

print('CSV IN FOLDER: ' + str(len(csvfiles)))

slist = []
cstring = ''

def listToString(s):
    slist = '\n'.join(map(str,s))
    #return print(str(slist))
    return str(slist)

# Create logs.csv if NOT existing
import os
d = os.path.dirname(__file__) # directory of script
p = r'{}/logs/'.format(d) # path to be created

try:
    os.makedirs(p)
    print(str(p))
except OSError:
    pass

#Create logs.txt if NOT existing
file_name = p+'/'+'logs.csv'
#print('file_name : ', file_name)
f = open(file_name, 'a+')  # open file in append mode
f.close()

'''with open(file_name, 'w', encoding='UTF8') as f:
    writer = csv.writer(f)
    # write the data
    writer.writerow(data)'''



#(adxID, adxGeo, cstring)
def webdrivers(id,geo,crid):
    web = webdriver.Chrome('chromedriver.exe')
    web.get('https://support.google.com/authorizedbuyers/contact/ghelp_contact_form_adxbuy')  # AdX Support Form Link
    time.sleep(2)  # 2s delay for UI loading
    WebDriverWait(web, 20).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="ghelp_contact_form_adxbuy"]/div[2]/div'))).click()  # Dropdown I'm having trouble with *
    time.sleep(0.05)
    WebDriverWait(web, 20).until(EC.element_to_be_clickable((By.XPATH, '//*[@id=":4"]'))).click()  # Select Creatives
    time.sleep(0.05)
    WebDriverWait(web, 20).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="ghelp_contact_form_adxbuy"]/div[5]/div'))).click()  # Dropdown Specifically *
    time.sleep(0.05)
    WebDriverWait(web, 20).until(
        EC.element_to_be_clickable((By.XPATH, '//*[@id=":26"]'))).click()  # Select Creative Status

    # Buyer creative IDs OPTIONAL, list creative hashes in description below

    # Is this creative for Russia or China? *
    time.sleep(0.05)
    WebDriverWait(web, 20).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="ghelp_contact_form_adxbuy"]/div[12]/div'))).click()  # Dropdown I'm having trouble with *
    time.sleep(0.05)
    WebDriverWait(web, 20).until(EC.element_to_be_clickable((By.XPATH, '//*[@id=":65"]'))).click()  # Select Creatives
    time.sleep(0.05)

    aID = web.find_element_by_xpath('//*[@id="account_id"]')
    aID.send_keys(id) # ADX ID

    # Subject *
    SubjectValue = str(id) + ' - ' + str(geo) + ' - PHARMA_GAMBLING_ALCOHOL_NOT_ALLOWED'  # Include Account Region beside RTB House RU, BR, APAC
    subject = web.find_element_by_xpath('//*[@id="subject_line"]')
    subject.send_keys(SubjectValue)
    #print('SubjectValue : '+ str(SubjectValue))

    # Full description *
    DescValue = 'Hello, We would like to check the status of each creative below and confirm if the actual rejection status is correct or just a false positive call. Please advise.\n\n' + str(crid)
    desc = web.find_element_by_xpath('//*[@id="problem_description"]')
    desc.send_keys(DescValue)

    # Name *
    nameValue = name1
    name = web.find_element_by_xpath('//*[@id="name"]')
    name.send_keys(nameValue)

    # Email address *
    emailValue = email1
    email = web.find_element_by_xpath('//*[@id="email"]')
    email.send_keys(emailValue)

    # Additional email addresses to be copied on this inquiry (To make sure bot works)
    ccemailValue = ccemails
    ccemail = web.find_element_by_xpath('//*[@id="additional_email"]')
    ccemail.send_keys(ccemailValue)

    # [ Radio button ] Can a Google representative contact you by phone about this issue? NO
    WebDriverWait(web, 20).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="ghelp_contact_form_adxbuy"]/div[36]/fieldset/div[2]/div/label/span'))).click()

    # [ Tick Box ] I understand
    WebDriverWait(web, 20).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="ghelp_contact_form_adxbuy"]/div[42]/fieldset/div/label/label/span[2]'))).click()

    time.sleep(2)

    # [ SUBMIT BUTTON CLICK ]
    # WebDriverWait(web, 20).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ghelp_contact_form_adxbuy"]/div[44]/div/button'))).click()
    # Uncomment line 138 to sent the email live; ATM it's disabled.

    # Confirmation email XPATH //*[@id="ghelp_contact_form_adxbuy"]/div/div[1]

for c in range(len(csvfiles)):
    #master.append(csvfiles[c])
    #print(master)
    sourceCsv = csvfiles[c];
    extension = os.path.splitext(sourceCsv)[0]
    df = pd.read_csv(sourceCsv)
    saved_column = df.hash_.tolist()
    master.append(saved_column)
    print('\n'+csvfiles[c])
    #print(len(master[c]))
    #print(csvfiles[c])
    #print(saved_column)
    #print('csv data count: ', len(saved_column), '\n')

    # csvfiles csv list unnested [ list1, list2 ]
    # main nested list [ [ list1 ], [ list2 ], [ list... ], ]

    #Merging variables
    csvs = csvfiles
    csvsIDs = master[c]
    cctr = len(csvs)
    #print('CSV: ' + str(len(csvs)))

    # acctIdValue = acctID
    # acctString = MAIN, APAC, PL, ETC

    #REGEX GEO
    #print('g c: ' + str(c))
    #print('g list: ' + str(gl[c]))

    #for gx = 0 (let i = 0; i <- len(geoList); i++):
    for gx in range(0, len(gl)):
        #print('gctr: ' + str(gx))

        if re.search(gl[gx], str(csvfiles[c])):
            # print('MATCH : '+geoList[g])
            #print('MATCH:  ' + str(gl[gx]))
            #print('CSV : ' + str(csvfiles[c]))

            y = str(gl[gx])
            # gl = ['ru', 'br', 'apac', 'tr', 'pl', 'jp']
            if y == 'ru':
                adxID = adxIDList[1]
                adxGeo = adxGeoList[1]
            elif y == 'br':
                adxID = adxIDList[2]
                adxGeo = adxGeoList[2]
            elif y == 'apac':
                adxID = adxIDList[3]
                adxGeo = adxGeoList[3]
            elif y == 'tr':
                adxID = adxIDList[4]
                adxGeo = adxGeoList[4]
            elif y == 'pl':
                adxID = adxIDList[5]
                adxGeo = adxGeoList[5]
            elif y == 'jp':
                adxID = adxIDList[6]
                adxGeo = adxGeoList[6]
            else:
                print(' NO MATCH ')
                continue
                adxID = adxIDList[0]
                adxGeo = adxGeoList[0]
            break
        else:
            adxID = adxIDList[0]
            adxGeo = adxGeoList[0]

        gx = gx + 1

    #geo = re.search(geoList, txt2Find)
    #print('geo: ' + geo)

    # COUNT ALL CSVS
    while cctr <= len(csvs):
        # print(csvs[cctr-1]) # CSV FILENAME
        print('ELEMENT: ' + str(len(csvsIDs)))
        ranges = len(csvsIDs)
        elem = len(csvsIDs)

        # CHECK IF CRIDS ARE MORE THAN 50
        if elem > 50:
            ranges = int(math.ceil(len(csvsIDs) / 50))
        else:
            ranges = 1
        print('RUN: ' + str(ranges))

        for e in range(ranges):
            # READ CSVS CRIDS
            sendList.clear()
            sendList = csvsIDs[:50]  # GET first 50 elements
            # print('SENDLIST: ' + str(len(sendList)))
            cstring = listToString(sendList)

            print(adxID + ' - ' + adxGeo + '\n' + str(len(sendList)))
            #print('CRIDs : \n' + str(cstring)) # UNCOMMENT TO SEE CRIDS BREAKDOWN

            # Add crids into logs.csv
            d = datetime.today().strftime('%Y-%m-%d')
            f = open(file_name, 'a+')
            f.write('\n')
            f.write(str(d))
            f.write('\n')
            f.write(str(cstring))
            f.close()

            #WEB DRIVER
            webdrivers(adxID,adxGeo,cstring)
            time.sleep(5) # DELAY BEFORE NEXT REPEATITION

            #print('ALLIST : ' + str(len(csvsIDs)))
            #print(str(csvsIDs))
            unqList = [i for i in csvsIDs + sendList if i not in csvsIDs or i not in sendList]  # GET REMAINING ELEMENTS
            csvsIDs.clear()
            csvsIDs = unqList.copy()
        #print('ALLIST : ' + str(len(csvsIDs)))
        # INCREMENT CTR
        cctr += 1

    time.sleep(2)